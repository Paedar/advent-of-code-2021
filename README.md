# Advent Of Code 2021

The advent of code is a yearly challenge to code some stuff. I personally use it to familiarize myself with new language features in Java.

To participate, please check Advent of Code 2021: https://adventofcode.com/2021

# Implementation details
I decided to implement the actual assignments in unit test cases. Obviously I do not do assignments by hand first to determine the expected values, but by saving my results in the test cases, I can reuse and alter code bits from previous assignments if I want to, while still making sure all previous assignment solutions work correctly.

# License
If you feel the need to copy my code, do so, but you won't be getting any better from doing so.
