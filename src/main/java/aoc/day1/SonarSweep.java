package aoc.day1;

import aoc.util.InputProcessor;
import aoc.util.ResourceReader;
import aoc.util.Util;

import java.util.List;
import java.util.stream.IntStream;

public class SonarSweep
        implements InputProcessor<Integer> {

    private List<Integer> readings;

    public int countNumberOfIncreases() {
        return countNumberOfIncreasesOn(readings);
    }

    private int countNumberOfIncreasesOn(List<Integer> input) {
        return (int) IntStream.range(0, input.size())
                              .filter(index -> isBiggerThanPredecessor(index, input))
                              .count();
    }

    public int countNumberOfIncreasesOfSlidingWindows(int slidingWindowSize) {
        var reducedInput = slidingWindowSum(slidingWindowSize, readings);
        return countNumberOfIncreasesOn(reducedInput);
    }

    private List<Integer> slidingWindowSum(int slidingWindowSize, List<Integer> input) {
        return Util.toSlidingWindow(input, slidingWindowSize)
                   .stream()
                   .map(this::sumList)
                   .toList();
    }

    private Integer sumList(List<Integer> integers) {
        return integers.stream().reduce(0, Integer::sum);
    }

    @Override
    public void readInput(String fileName) {
        this.readings = ResourceReader.parseIntegers(fileName);
    }

    private boolean isBiggerThanPredecessor(int index, List<Integer> input) {
        return index > 0 && index < input.size() && input.get(index) > input.get(index - 1);
    }

}
