package aoc.util;

public interface InputProcessor<T> {
    void readInput(String fileName);
}
