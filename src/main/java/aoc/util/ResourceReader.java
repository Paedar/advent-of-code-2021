package aoc.util;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class ResourceReader {

    private ResourceReader () {
        // hide constructor
    }

    public static List<String> readInput(String fileName) {
        try {
            var resourceUri = ClassLoader.getSystemResource(fileName).toURI();
            var path = Paths.get(resourceUri);
            return Files.readAllLines(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public static <T> List<T> parseLines(String fileName, Function<String, T> lineParser) {
        var lines = readInput(fileName);
        return lines.stream().map(lineParser).toList();
    }

    public static List<Integer> parseIntegers(String fileName) {
        return parseLines(fileName, Integer::valueOf);
    }

}
