package aoc.util;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Util {

    private Util() {
        // hide constructor
    }

    public static <T> List<List<T>> toSlidingWindow(List<T> in, int slidingWindowSize) {
        if (slidingWindowSize > in.size()) {
            return List.of();
        }
        return IntStream.range(0, in.size() - slidingWindowSize + 1)
                        .sequential()
                        .mapToObj(start -> in.subList(start, start + slidingWindowSize))
                        .toList();
    }

    public static <T> Map<T, Long> countOccurrences(List<T> in) {
        return in.stream().collect(Collectors.groupingBy(it -> it, Collectors.counting()));
    }

    public static <T> T mostOccurring(List<T> in) {
        var countMap = countOccurrences(in);
        return countMap.entrySet().stream()
                       .max(Comparator.comparingLong(Map.Entry::getValue))
                       .map(Map.Entry::getKey)
                       .orElse(null);
    }

    public static <T> Set<T> mostOccurringMulti(List<T> in) {
        var countMap = countOccurrences(in);
        var mostOccurringCount = countMap.entrySet().stream()
                                         .max(Comparator.comparingLong(Map.Entry::getValue))
                                         .map(Map.Entry::getValue)
                                         .orElse(-1L);
        return countMap.entrySet()
                       .stream()
                       .filter(it -> mostOccurringCount.equals(it.getValue()))
                       .map(Map.Entry::getKey)
                       .collect(Collectors.toSet());
    }

}
