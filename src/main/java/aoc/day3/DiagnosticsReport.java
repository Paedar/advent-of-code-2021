package aoc.day3;

public record DiagnosticsReport(DiagnosticsReading gamma, DiagnosticsReading epsilon, DiagnosticsReading oxygenGenerator, DiagnosticsReading co2Scrubber) {
    public int getPower() {
        return gamma.asInteger() * epsilon.asInteger();
    }

    public int getLifeSupportRating() {
        return oxygenGenerator.asInteger() * co2Scrubber.asInteger();
    }
}
