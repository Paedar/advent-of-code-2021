package aoc.day3;

import java.util.List;
import java.util.stream.Collectors;

public record DiagnosticsReading(List<Bit> reading) {

    public static DiagnosticsReading from(String in) {
        var reading = in.chars().mapToObj(Bit::of).toList();
        return new DiagnosticsReading(reading);
    }

    public static DiagnosticsReading asEpsilonReading(DiagnosticsReading gammaReading) {
        var newReading = gammaReading.reading().stream().map(Bit::flip).toList();
        return new DiagnosticsReading(newReading);
    }

    public int length() {
        return reading.size();
    }

    public int asInteger() {
        var binary = reading.stream().map(Bit::toString).collect(Collectors.joining());
        return Integer.parseInt(binary, 2);
    }

}
