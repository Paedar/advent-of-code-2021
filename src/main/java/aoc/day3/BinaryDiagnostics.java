package aoc.day3;

import aoc.util.InputProcessor;
import aoc.util.ResourceReader;
import aoc.util.Util;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static aoc.day3.Bit.ONE;

public class BinaryDiagnostics
        implements InputProcessor<DiagnosticsReading> {

    private List<DiagnosticsReading> readings;

    @Override
    public void readInput(String fileName) {
        readings = ResourceReader.parseLines(fileName, DiagnosticsReading::from);
    }

    public DiagnosticsReport getReport() {
        var gammaReading = gamma();
        var epsilonReading = DiagnosticsReading.asEpsilonReading(gammaReading);

        var oxygenGeneratorReading = oxygenGenerator();
        var co2ScrubberReading = co2Scrubber();

        return new DiagnosticsReport(gammaReading, epsilonReading, oxygenGeneratorReading, co2ScrubberReading);
    }

    private DiagnosticsReading gamma() {
        var mostOccurrencesPerPosition = readingsAtPositions().map(Util::mostOccurring).toList();
        return new DiagnosticsReading(mostOccurrencesPerPosition);
    }

    private DiagnosticsReading oxygenGenerator() {
        return bitwiseFilterToSingle(this::oxygenGeneratorBitCriteria);
    }

    private DiagnosticsReading co2Scrubber() {
        return bitwiseFilterToSingle(this::co2ScrubberBitCriteria);
    }

    private Bit oxygenGeneratorBitCriteria(Integer position, List<DiagnosticsReading> currentList) {
        var bitsAtPositions = readingsAtPosition(position, currentList);
        var mostOccurringMulti = Util.mostOccurringMulti(bitsAtPositions);
        if (mostOccurringMulti.size() == 1) {
            return mostOccurringMulti.stream().findFirst().get();
        }
        return ONE;
    }

    private Bit co2ScrubberBitCriteria(Integer position, List<DiagnosticsReading> currentList) {
        return oxygenGeneratorBitCriteria(position, currentList).flip();
    }

    private Predicate<DiagnosticsReading> bitOnPosition(int position, List<DiagnosticsReading> filteredList, BiFunction<Integer, List<DiagnosticsReading>, Bit> bitCriteria) {
        return it -> it.reading().get(position).equals(bitCriteria.apply(position, filteredList));
    }

    private DiagnosticsReading bitwiseFilterToSingle(BiFunction<Integer, List<DiagnosticsReading>, Bit> bitCriteria) {
        var currentList = List.copyOf(readings);
        var readingLength = minReadingLength();
        var position = 0;
        while (currentList.size() > 1 && position < readingLength) {
            currentList = currentList.stream()
                                     .filter(bitOnPosition(position, currentList, bitCriteria))
                                     .toList();
            ++position;
        }
        if (currentList.size() == 1) {
            return currentList.get(0);
        }
        throw new RuntimeException("Passed over all bits, but still have " + currentList.size() + " results.");
    }

    private Stream<List<Bit>> readingsAtPositions() {
        return IntStream.range(0, minReadingLength())
                        .mapToObj(this::readingsAtPosition);
    }

    private List<Bit> readingsAtPosition(int position) {
        return readingsAtPosition(position, this.readings);
    }

    private List<Bit> readingsAtPosition(int position, List<DiagnosticsReading> readings) {
        return readings.stream().map(DiagnosticsReading::reading).map(it -> it.get(position)).toList();
    }

    private int minReadingLength() {
        return readings.stream()
                       .mapToInt(DiagnosticsReading::length)
                       .min()
                       .orElse(0);
    }

}
