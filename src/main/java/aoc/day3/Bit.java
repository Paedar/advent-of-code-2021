package aoc.day3;

public enum Bit {
    ZERO,
    ONE
    ;

    public static Bit of(int bitValue) {
        if(bitValue > 255)
            throw new NumberFormatException("Invalid bit");
        return of((char) bitValue);
    }

    public static Bit of(char bitValue) {
        return switch(bitValue) {
            case '0' -> ZERO;
            case '1' -> ONE;
            default -> throw new NumberFormatException("Invalid bit");
        };
    }

    public Bit flip() {
        return switch(this) {
            case ZERO -> ONE;
            case ONE -> ZERO;
        };
    }

    @Override
    public String toString() {
        return switch(this) {
            case ZERO -> "0";
            case ONE -> "1";
        };
    }

}
