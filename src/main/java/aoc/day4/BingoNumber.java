package aoc.day4;

import java.util.Objects;

public class BingoNumber {

    private final int number;

    private boolean marked;

    private BingoNumber(int number, boolean marked) {
        this.number = number;
        this.marked = marked;
    }

    public static BingoNumber unmarked(int number) {
        return new BingoNumber(number, false);
    }

    public boolean isMarked() {
        return marked;
    }

    public void markIf(int number) {
        if(this.number == number)
            this.mark();
    }

    public int getNumber() {
        return number;
    }

    private void mark() {
        marked = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BingoNumber that = (BingoNumber) o;
        return number == that.number && marked == that.marked;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, marked);
    }

    @Override
    public String toString() {
        return "BN{" + number +
                       ", " + marked +
                       '}';
    }

}
