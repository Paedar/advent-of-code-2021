package aoc.day4;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;

public class BingoCard {

    public static final int BINGO_SIZE = 5;

    private final List<BingoNumber> numbers;

    private boolean finished;

    private int finishedAt;

    private BingoCard(List<BingoNumber> numbers) {
        this.numbers = List.copyOf(numbers);
    }

    public static BingoCard ofLines(List<String> lines) {
        if (lines.size() != 5) {
            throw new UnsupportedOperationException("Incorrect amount of lines for a bingo card");
        }
        var bingoLines = lines.stream()
                              .sequential()
                              .map(it -> it.split(" +"))
                              .map(List::of)
                                 .map(BingoCard::correctForEmpty)
                              .toList();

        bingoLines.forEach(it -> {
            if (it.size() != BINGO_SIZE) {
                throw new UnsupportedOperationException("Incorrect amount of items on line");
            }
        });

        var bingoNumbers = bingoLines.stream()
                                     .flatMap(List::stream)
                                     .map(Integer::valueOf)
                                     .map(BingoNumber::unmarked)
                                     .toList();
        return new BingoCard(bingoNumbers);
    }

    public void mark(int number) {
        if (!finished) {
            numbers.stream().filter(not(BingoNumber::isMarked))
                   .forEach(it -> it.markIf(number));

            if (anyRowOrColumnFinished()) {
                this.finish(number);
            }
        }
    }

    public boolean isFinished() {
        return finished;
    }

    public List<BingoLine> getAllLinesAndColumns() {
        return IntStream.range(0, BINGO_SIZE)
                        .mapToObj(it -> Stream.of(getRow(it), getColumn(it)))
                        .flatMap(it -> it)
                        .toList();
    }

    public int getCardScore() {
        if(!isFinished()) {
            return -1;
        }
        return finishedAt * sumOfUnmarkedNumbers();
    }

    public int getFinishedAt() {
        return finishedAt;
    }

    private int sumOfUnmarkedNumbers() {
        return this.numbers.stream().filter(not(BingoNumber::isMarked)).map(BingoNumber::getNumber).reduce(0, Integer::sum);
    }

    private boolean anyRowOrColumnFinished() {
        return getAllLinesAndColumns().stream().anyMatch(BingoLine::isFinished);
    }

    private BingoLine getRow(int rowNumber) {
        if (rowNumber < 0 || rowNumber > BINGO_SIZE - 1) {
            throw new UnsupportedOperationException("Invalid row number");
        }

        var rowNumbers = numbers.subList(BINGO_SIZE * rowNumber, BINGO_SIZE * (rowNumber + 1));
        return new BingoLine(rowNumbers);
    }

    private BingoLine getColumn(int columnNumber) {
        if (columnNumber < 0 || columnNumber > BINGO_SIZE - 1) {
            throw new UnsupportedOperationException("Invalid row number");
        }

        var rowNumbers = IntStream.range(0, BINGO_SIZE)
                                  .map(it -> it * BINGO_SIZE + columnNumber)
                                  .mapToObj(numbers::get)
                                  .toList();
        return new BingoLine(rowNumbers);
    }

    private void finish(int at) {
        finishedAt = at;
        finished = true;
    }

    private static List<String> correctForEmpty(List<String> splitNumbers) {
        return splitNumbers.stream().filter(it -> !"".equals(it)).toList();
    }

    record BingoLine(List<BingoNumber> numbers) {
        boolean isFinished() {
            return numbers.stream().allMatch(BingoNumber::isMarked);
        }
    }

}
