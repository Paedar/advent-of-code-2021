package aoc.day4;

import aoc.util.InputProcessor;
import aoc.util.ResourceReader;

import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static aoc.day4.BingoCard.BINGO_SIZE;
import static java.util.function.Predicate.not;

public class Bingo implements InputProcessor<BingoCard> {

    private List<Integer> numbers;
    private List<BingoCard> cards;

    @Override
    public void readInput(String fileName) {
        var lines = ResourceReader.readInput(fileName);

        numbers = Stream.of(lines.get(0).split(","))
                              .map(Integer::valueOf)
                              .toList();
        lines.remove(0);

        var numberOfBingoCards = lines.size() / (BINGO_SIZE + 1);

        cards = IntStream.range(0, numberOfBingoCards)
                .mapToObj(it -> readCard(lines, it))
                .toList();
    }

    private BingoCard readCard(List<String> lines, int cardNumber) {
        var linesPerCard = BINGO_SIZE + 1;
        var startIndex = cardNumber * linesPerCard + 1;
        var cardLines = lines.subList(startIndex, startIndex + BINGO_SIZE);
        return BingoCard.ofLines(cardLines);
    }

    public BingoCard firstFinishingCard() {
        var numberIndex = 0;
        while(Objects.isNull(getFinishedCard()) && numberIndex < numbers.size()) {
            var number = numbers.get(numberIndex);
            cards.forEach(card -> card.mark(number));
            ++numberIndex;
        }

        return getFinishedCard();
    }

    public BingoCard lastFinishingCard() {
        var numberIndex = 0;
        int number = Integer.MIN_VALUE;
        while(numberOfUnfinishedCards() != 0 && numberIndex < numbers.size()) {
            number = numbers.get(numberIndex);
            for(var card : cards) {
                card.mark(number);
            }
            ++numberIndex;
        }
        int finalNumber = number;
        return cards.stream().filter(it -> it.getFinishedAt() == finalNumber).findFirst().orElse(null);
    }

    public List<BingoCard> getCards() {
        return cards;
    }

    public long numberOfUnfinishedCards() {
        return cards.stream().filter(not(BingoCard::isFinished)).count();
    }

    public List<Integer> getNumbers() {
        return numbers;
    }

    private BingoCard getFinishedCard() {
        return cards.stream().filter(BingoCard::isFinished).findFirst().orElse(null);
    }

}
