package aoc.day5;

public record Point(int x, int y) {

    public static Point of(String pointString) {
        var coordinates = pointString.split(",");
        if(coordinates.length != 2) {
            throw new UnsupportedOperationException("Incorrect format for coordinates");
        }
        var x = Integer.valueOf(coordinates[0]);
        var y = Integer.valueOf(coordinates[1]);
        return new Point(x, y);
    }

}
