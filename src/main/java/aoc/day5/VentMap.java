package aoc.day5;

import aoc.util.InputProcessor;
import aoc.util.ResourceReader;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class VentMap
        implements InputProcessor<Vent> {

    List<Vent> vents;

    @Override
    public void readInput(String fileName) {
        this.vents = ResourceReader.parseLines(fileName, Vent::of);
    }

    public long countOverlappingHorizontalVerticalVentPoints() {
        return countOverlappingPoints(Vent::isHorizontalOrVertical);
    }

    public long countOverlappingVentPoints() {
        return countOverlappingPoints(it -> true);
    }

    private long countOverlappingPoints(Predicate<Vent> condition) {
        return getPointMap(condition)
                .entrySet()
                .stream()
                .filter(it -> it.getValue() > 1)
                .count();
    }

    private Map<Point, Long> getPointMap(Predicate<Vent> condition) {
        return vents.stream()
                    .filter(condition)
                    .map(Vent::toPoints)
                    .flatMap(Set::stream)
                    .collect(Collectors.groupingBy(it -> it, Collectors.counting()));
    }

}
