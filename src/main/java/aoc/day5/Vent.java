package aoc.day5;

import java.util.HashSet;
import java.util.Set;

public record Vent(Point start, Point end) {

    public static Vent of(String vent) {
        var pointStrings = vent.split(" +-> +");
        if(pointStrings.length != 2) {
            throw new UnsupportedOperationException("Formatting incorrect");
        }
        var p1 = Point.of(pointStrings[0]);
        var p2 = Point.of(pointStrings[1]);
        return new Vent(p1, p2);
    }

    public boolean isVertical() {
        return start.x() == end.x();
    }

    public boolean isHorizontal() {
        return start.y() == end.y();
    }

    public boolean isHorizontalOrVertical() {
        return isHorizontal() || isVertical();
    }

    public Set<Point> toPoints() {
        var dx = end.x() - start.x();
        var dy = end.y() - start.y();

        var dist = Math.max(Math.abs(dx), Math.abs(dy));
        var stepX = dx / dist; // Determines sign
        var stepY = dy / dist; // Determines sign

        var points = new HashSet<Point>();
        for(var step = 0; step < dist + 1; ++step) {
            var newX = start.x() + stepX * step;
            var newY = start.y() + stepY * step;
            var newPoint = new Point(newX, newY);
            points.add(newPoint);
        }
        return points;
    }

}
