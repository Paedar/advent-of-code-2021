package aoc.day2;

public final class AimedPosition extends Position {
    private final Integer aim;

    public AimedPosition() {
        this(0,0,0);
    }

    public AimedPosition(int horizontal, int depth, int aim) {
        super(horizontal, depth);
        this.aim = aim;
    }

    @Override
    public Position apply(Instruction instruction) {
        return switch(instruction.direction()) {
            case FORWARD -> new AimedPosition(horizontal + instruction.amount(), depth + aim * instruction.amount(), aim);
            case DOWN -> new AimedPosition(horizontal, depth, aim + instruction.amount());
            case UP -> new AimedPosition(horizontal, depth, aim - instruction.amount());
        };
    }

}
