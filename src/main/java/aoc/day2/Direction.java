package aoc.day2;

public enum Direction {
    FORWARD,
    DOWN,
    UP;

    public static Direction from(String in) {
        return switch (in) {
            case "forward" -> FORWARD;
            case "down" -> DOWN;
            case "up" -> UP;
            default -> throw new UnsupportedOperationException();
        };
    }
}
