package aoc.day2;

import aoc.util.InputProcessor;
import aoc.util.ResourceReader;

import java.util.List;

public class Movement implements InputProcessor<Instruction> {

    private List<Instruction> instructions;

    @Override
    public void readInput(String fileName) {
        instructions = ResourceReader.parseLines(fileName, Instruction::from);
    }

    public Position moveMe(Position startPosition) {
        var position = startPosition;

        for(var instruction : instructions) {
            position = position.apply(instruction);
        }

        return position;
    }

}
