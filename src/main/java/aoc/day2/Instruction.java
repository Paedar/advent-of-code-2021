package aoc.day2;

public record Instruction(Direction direction, Integer amount) {
    public static Instruction from(String instruction) {
        var parts = instruction.split(" ");
        if(parts.length != 2)
            throw new UnsupportedOperationException();

        var direction = Direction.from(parts[0]);
        var movement = Integer.valueOf(parts[1]);

        return new Instruction(direction, movement);
    }
}
