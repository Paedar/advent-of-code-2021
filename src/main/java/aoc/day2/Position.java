package aoc.day2;

import java.util.Objects;

public sealed class Position
        permits AimedPosition {

    protected final Integer horizontal;

    protected final Integer depth;

    public Position(Integer horizontal, Integer depth) {
        this.horizontal = horizontal;
        this.depth = depth;
    }

    Position() {
        this(0, 0);
    }

    int product() {
        return horizontal * depth;
    }

    public Integer horizontal() {
        return horizontal;
    }

    public Integer depth() {
        return depth;
    }

    public Position apply(Instruction instruction) {
        return switch(instruction.direction()) {
            case FORWARD -> new Position(this.horizontal() + instruction.amount(), this.depth());
            case DOWN -> new Position(this.horizontal(), this.depth() + instruction.amount());
            case UP -> new Position(this.horizontal(), this.depth() - instruction.amount());
        };
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Position) obj;
        return Objects.equals(this.horizontal, that.horizontal) &&
                       Objects.equals(this.depth, that.depth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(horizontal, depth);
    }

    @Override
    public String toString() {
        return "Position[" +
                       "horizontal=" + horizontal + ", " +
                       "depth=" + depth + ']';
    }

}
