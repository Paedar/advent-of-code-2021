package aoc.day1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SonarSweepTest {

    private SonarSweep sweep;

    @BeforeEach
    void initializeSweep() {
        sweep = new SonarSweep();
    }

    @Test
    void countNumberOfIncreasesOnExample() {
        sweep.readInput("1.example");
        var output = sweep.countNumberOfIncreases();
        assertEquals(7, output);
    }

    @Test
    void countNumberOfIncreasesOnExampleUsingSlidingWindow() {
        sweep.readInput("1.example");
        var output = sweep.countNumberOfIncreasesOfSlidingWindows(1);
        assertEquals(7, output);
    }

    @Test
    void countNumberOfIncreasesOnExampleWithSlidingWindow() {
        sweep.readInput("1.example");
        var output = sweep.countNumberOfIncreasesOfSlidingWindows(3);
        assertEquals(5, output);
    }

    @Test
    void countNumberOfIncreasesOnInput() {
        sweep.readInput("1.input");
        var output = sweep.countNumberOfIncreases();
        System.out.println("Amount of increases: " + output);
        assertEquals(1266, output);
    }

    @Test
    void countNumberOfIncreasesOnInputUsingSlidingWindow() {
        sweep.readInput("1.input");
        var output = sweep.countNumberOfIncreasesOfSlidingWindows(1);
        assertEquals(1266, output);
    }

    @Test
    void countNumberOfIncreasesOnInputWithSlidingWindow() {
        sweep.readInput("1.input");
        var output = sweep.countNumberOfIncreasesOfSlidingWindows(3);
        System.out.println("Amount of increases: " + output);
        assertEquals(1217, output);
    }

}
