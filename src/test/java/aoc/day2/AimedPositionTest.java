package aoc.day2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static aoc.day2.Direction.DOWN;
import static aoc.day2.Direction.FORWARD;
import static aoc.day2.Direction.UP;
import static org.junit.jupiter.api.Assertions.*;

class AimedPositionTest {


    AimedPosition startPosition;

    @BeforeEach
    void initStart() {
        startPosition = new AimedPosition();
    }

    @Test
    void applyForward() {
        var movement = 5;
        var instruction = new Instruction(FORWARD, movement);
        var end = startPosition.apply(instruction);

        assertEquals(new AimedPosition(movement, 0, 0), end);
    }

    @Test
    void applyDown() {
        var movement = 5;
        var instruction = new Instruction(DOWN, movement);
        var end = startPosition.apply(instruction);

        assertEquals(new AimedPosition(0, 0, movement), end);
    }

    @Test
    void applyUp() {
        var movement = 5;
        var instruction = new Instruction(UP, movement);
        var end = startPosition.apply(instruction);

        assertEquals(new AimedPosition(0, 0,-movement), end);
    }

    @Test
    void applySequenceWithDown() {
        var aim = 5;
        var instruction = new Instruction(DOWN, aim);
        var next = startPosition.apply(instruction);
        assertEquals(new AimedPosition(0, 0,aim), next);

        var forwardMovement = 2;
        instruction = new Instruction(FORWARD, forwardMovement);
        next = next.apply(instruction);
        assertEquals(new AimedPosition(forwardMovement, forwardMovement * aim, aim), next);
    }

    @Test
    void applySequenceWithUp() {
        var aim = 5;
        var instruction = new Instruction(UP, aim);
        var next = startPosition.apply(instruction);
        assertEquals(new AimedPosition(0, 0, -aim), next);

        var forwardMovement = 2;
        instruction = new Instruction(FORWARD, forwardMovement);
        next = next.apply(instruction);
        assertEquals(new AimedPosition(forwardMovement, -forwardMovement * aim, -aim), next);
    }
}
