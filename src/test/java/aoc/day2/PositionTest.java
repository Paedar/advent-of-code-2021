package aoc.day2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static aoc.day2.Direction.DOWN;
import static aoc.day2.Direction.FORWARD;
import static aoc.day2.Direction.UP;
import static org.junit.jupiter.api.Assertions.*;

class PositionTest {


    Position startPosition;

    @BeforeEach
    void initStart() {
        startPosition = new Position();
    }

    @Test
    void applyForward() {
        var movement = 5;
        var instruction = new Instruction(FORWARD, movement);
        var end = startPosition.apply(instruction);

        assertEquals(new Position(movement, 0), end);
    }

    @Test
    void applyDown() {
        var movement = 5;
        var instruction = new Instruction(DOWN, movement);
        var end = startPosition.apply(instruction);

        assertEquals(new Position(0, movement), end);
    }

    @Test
    void applyUp() {
        var movement = 5;
        var instruction = new Instruction(UP, movement);
        var end = startPosition.apply(instruction);

        assertEquals(new Position(0, -movement), end);
    }

}
