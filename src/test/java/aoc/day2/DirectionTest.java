package aoc.day2;

import org.junit.jupiter.api.Test;

import static aoc.day2.Direction.DOWN;
import static aoc.day2.Direction.FORWARD;
import static aoc.day2.Direction.UP;
import static org.junit.jupiter.api.Assertions.*;

class DirectionTest {

    @Test
    void from_correct() {
        var direction = Direction.from("forward");
        assertEquals(FORWARD, direction);
        direction = Direction.from("down");
        assertEquals(DOWN, direction);
        direction = Direction.from("up");
        assertEquals(UP, direction);
    }

    @Test
    void from_incorrect() {
        assertThrows(UnsupportedOperationException.class, () -> Direction.from("iets anders"));
    }

}
