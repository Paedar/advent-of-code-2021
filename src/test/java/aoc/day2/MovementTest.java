package aoc.day2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MovementTest {

    Movement movement;

    @BeforeEach
    void initializeMovement() {
        movement = new Movement();
    }

    @Test
    void moveMeOnExample() {
        movement.readInput("2.example");
        var finalPosition = movement.moveMe(new Position());
        assertEquals(new Position(15, 10), finalPosition);
        assertEquals(150, finalPosition.product());
    }

    @Test
    void moveMeOnInput() {
        movement.readInput("2.input");
        var finalPosition = movement.moveMe(new Position());
        System.out.println("Final Position: " + finalPosition);
        System.out.println("Product: " + finalPosition.product());
        assertEquals(new Position(1925, 879), finalPosition);
        assertEquals(1692075, finalPosition.product());
    }

    @Test
    void moveMeAimedOnExample() {
        movement.readInput("2.example");
        var finalPosition = movement.moveMe(new AimedPosition());
        assertEquals(new AimedPosition(15, 60, 0), finalPosition);
        assertEquals(900, finalPosition.product());
    }

    @Test
    void moveMeAimedOnInput() {
        movement.readInput("2.input");
        var finalPosition = movement.moveMe(new AimedPosition());
        System.out.println("Final Position: " + finalPosition);
        System.out.println("Product: " + finalPosition.product());
        assertEquals(new AimedPosition(1925, 908844, 0), finalPosition);
        assertEquals(1749524700, finalPosition.product());
    }


}
