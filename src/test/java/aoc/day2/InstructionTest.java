package aoc.day2;

import org.junit.jupiter.api.Test;

import static aoc.day2.Direction.DOWN;
import static aoc.day2.Direction.FORWARD;
import static aoc.day2.Direction.UP;
import static org.junit.jupiter.api.Assertions.*;

class InstructionTest {

    @Test
    void fromStringCorrect() {
        var instruction = Instruction.from("forward 5");
        assertEquals(new Instruction(FORWARD, 5), instruction);
        instruction = Instruction.from("down 3");
        assertEquals(new Instruction(DOWN, 3), instruction);
        instruction = Instruction.from("up 6");
        assertEquals(new Instruction(UP, 6), instruction);
    }

    @Test
    void incorrectFormat() {
        assertThrows(UnsupportedOperationException.class, () -> Instruction.from("asb"));
        assertThrows(UnsupportedOperationException.class, () -> Instruction.from("asb 1 1"));
        assertThrows(UnsupportedOperationException.class, () -> Instruction.from("asb 1"));
        assertThrows(NumberFormatException.class, () -> Instruction.from("forward as"));
    }

}
