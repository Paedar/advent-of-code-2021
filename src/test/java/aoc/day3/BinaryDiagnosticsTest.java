package aoc.day3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinaryDiagnosticsTest {

    private BinaryDiagnostics diagnostics;

    @BeforeEach
    void initialize() {
        diagnostics = new BinaryDiagnostics();
    }

    @Test
    void getReportOnExample() {
        diagnostics.readInput("3.example");
        var report = diagnostics.getReport();
        var expectedReport = new DiagnosticsReport(DiagnosticsReading.from("10110"), DiagnosticsReading.from("01001"), DiagnosticsReading.from("10111"), DiagnosticsReading.from("01010"));
        assertEquals(expectedReport, report);
        assertEquals(198, report.getPower());
        assertEquals(230, report.getLifeSupportRating());
    }

    @Test
    void getReportOnInput() {
        diagnostics.readInput("3.input");
        var report = diagnostics.getReport();
        var expectedReport = new DiagnosticsReport(DiagnosticsReading.from("011100011000"), DiagnosticsReading.from("100011100111"), DiagnosticsReading.from("011111101111"), DiagnosticsReading.from("100000111000"));
        assertEquals(expectedReport, report);
        assertEquals(4138664, report.getPower());
        assertEquals(4273224, report.getLifeSupportRating());
    }

}
