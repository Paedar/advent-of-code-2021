package aoc.day3;

import org.junit.jupiter.api.Test;

import java.util.List;

import static aoc.day3.Bit.ONE;
import static aoc.day3.Bit.ZERO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DiagnosticsReadingTest {


    @Test
    void from() {
        var diagnosticsReading = DiagnosticsReading.from("101");
        assertEquals(new DiagnosticsReading(List.of(ONE, ZERO, ONE)), diagnosticsReading);
        diagnosticsReading = DiagnosticsReading.from("");
        assertEquals(new DiagnosticsReading(List.of()), diagnosticsReading);
    }

    @Test
    void fromExceptional() {
        assertThrows(NumberFormatException.class, () -> DiagnosticsReading.from("abc"));
    }

    @Test
    void asEpsilonReading() {
        var gammaReading = DiagnosticsReading.from("101");
        var expectedEpsilonReading = DiagnosticsReading.from("010");
        var actualEpsilonReading = DiagnosticsReading.asEpsilonReading(gammaReading);
        assertEquals(expectedEpsilonReading, actualEpsilonReading);
    }

    @Test
    void length() {
        var diagnosticsReading = DiagnosticsReading.from("101");
        assertEquals(3, diagnosticsReading.length());
    }

    @Test
    void asInt() {
        var diagnosticsReading = DiagnosticsReading.from("101");
        assertEquals(5, diagnosticsReading.asInteger());
        diagnosticsReading = DiagnosticsReading.from("0101");
        assertEquals(5, diagnosticsReading.asInteger());
    }

}
