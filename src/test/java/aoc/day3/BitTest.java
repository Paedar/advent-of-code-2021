package aoc.day3;

import org.junit.jupiter.api.Test;

import static aoc.day3.Bit.ONE;
import static aoc.day3.Bit.ZERO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BitTest {

    @Test
    void ofInt() {
        var bit = Bit.of(0);
        assertEquals(ZERO, bit);
        bit = Bit.of(1);
        assertEquals(ONE, bit);
        assertThrows(NumberFormatException.class, () -> Bit.of(-1));
        assertThrows(NumberFormatException.class, () -> Bit.of(2));
        assertThrows(NumberFormatException.class, () -> Bit.of(256));
    }

    @Test
    void ofChar() {
        var bit = Bit.of('0');
        assertEquals(ZERO, bit);
        bit = Bit.of('1');
        assertEquals(ONE, bit);
        assertThrows(NumberFormatException.class, () -> Bit.of('2'));
    }

    @Test
    void flip() {
        var bit = Bit.of('0');
        assertEquals(ZERO, bit);
        bit = bit.flip();
        assertEquals(ONE, bit);
        bit = bit.flip();
        assertEquals(ZERO, bit);
    }

    @Test
    void testToString() {
        assertEquals("0", ZERO.toString());
        assertEquals("1", ONE.toString());
    }

}
