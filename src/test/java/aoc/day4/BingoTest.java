package aoc.day4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BingoTest {

    private Bingo bingo;

    @BeforeEach
    void initBingo() {
        bingo = new Bingo();
    }

    @Test
    void sanityCheckInputOnExample() {
        bingo.readInput("4.example");
        assertEquals(27, bingo.getNumbers().size());
        assertEquals(3, bingo.getCards().size());
        assertEquals(3, bingo.numberOfUnfinishedCards());
    }

    @Test
    void firstFinishingCardOnExample() {
        bingo.readInput("4.example");
        var finishedCard = bingo.firstFinishingCard();
        assertNotNull(finishedCard);
        assertEquals(4512, finishedCard.getCardScore());
        assertEquals(2, bingo.numberOfUnfinishedCards());
    }

    @Test
    void firstFinishingCardOnInput() {
        bingo.readInput("4.input");
        var finishedCard = bingo.firstFinishingCard();
        assertNotNull(finishedCard);
        assertEquals(39984, finishedCard.getCardScore());
    }

    @Test
    void lastFinishingCardOnExample() {
        bingo.readInput("4.example");
        var finishedCard = bingo.lastFinishingCard();
        assertNotNull(finishedCard);
        assertEquals(0, bingo.numberOfUnfinishedCards());
        assertEquals(1924, finishedCard.getCardScore());
    }

    @Test
    void lastFinishingCardOnInput() {
        bingo.readInput("4.input");
        var finishedCard = bingo.lastFinishingCard();
        assertNotNull(finishedCard);
        assertEquals(0, bingo.numberOfUnfinishedCards());
        assertEquals(8468, finishedCard.getCardScore());
    }



}
