package aoc.day4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BingoNumberTest {

    @Test
    void unmarked() {
        var bingoNumber = BingoNumber.unmarked(1);
        assertEquals(1, bingoNumber.getNumber());
        assertFalse(bingoNumber.isMarked());
        bingoNumber = BingoNumber.unmarked(2);
        assertEquals(2, bingoNumber.getNumber());
        assertFalse(bingoNumber.isMarked());
    }

    @Test
    void markIf() {
        var bingoNumber = BingoNumber.unmarked(1);
        assertEquals(1, bingoNumber.getNumber());
        assertFalse(bingoNumber.isMarked());

        bingoNumber.markIf(2);
        assertFalse(bingoNumber.isMarked());
        bingoNumber.markIf(1);
        assertTrue(bingoNumber.isMarked());
    }

}
