package aoc.day4;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BingoCardTest {

    private static final List<String> BINGO_CARD = List.of(
            " 1  2  3  4  5",
            " 6  7  8  9 10",
            "11 12 13 14 15",
            "16 17 18 19 20",
            "21 22 23 24 25");

    @Test
    void ofLinesDealsWithMultipleSpacing() {
        var card = BingoCard.ofLines(BINGO_CARD);
        assertNotNull(card);
    }

    @Test
    void mark() {
        var card = BingoCard.ofLines(BINGO_CARD);

        var markNumber = 16;
        assertFalse(isMarkedOnCard(card, markNumber));
        card.mark(markNumber);
        assertTrue(isMarkedOnCard(card, markNumber));
    }

    @Test
    void markOnlyWhileNotFinished() {
        var card = BingoCard.ofLines(BINGO_CARD);

        card.mark(1);
        card.mark(2);
        card.mark(3);
        card.mark(4);
        card.mark(5);

        assertFalse(isMarkedOnCard(card, 6));
        card.mark(6);
        assertFalse(isMarkedOnCard(card, 6));
    }

    @Test
    void finishOnColumn() {
        var card = BingoCard.ofLines(BINGO_CARD);

        card.mark(1);
        assertFalse(card.isFinished());
        card.mark(6);
        assertFalse(card.isFinished());
        card.mark(11);
        assertFalse(card.isFinished());
        card.mark(16);
        assertFalse(card.isFinished());
        card.mark(21);
        assertTrue(card.isFinished());

        assertEquals(21, card.getFinishedAt());
    }

    @Test
    void finishOnRow() {
        var card = BingoCard.ofLines(BINGO_CARD);

        card.mark(1);
        assertFalse(card.isFinished());
        card.mark(2);
        assertFalse(card.isFinished());
        card.mark(3);
        assertFalse(card.isFinished());
        card.mark(4);
        assertFalse(card.isFinished());
        card.mark(5);
        assertTrue(card.isFinished());

        assertEquals(5, card.getFinishedAt());
    }

    private static boolean isMarkedOnCard(BingoCard card, int number) {
        return card.getAllLinesAndColumns().stream()
                .map(BingoCard.BingoLine::numbers)
                .flatMap(Collection::stream)
                .filter(it -> it.getNumber() == number)
                .allMatch(BingoNumber::isMarked);
    }

    @Test
    void getAllLinesAndColumns() {
        var card = BingoCard.ofLines(BINGO_CARD);

        var allLinesAndColumns = card.getAllLinesAndColumns();
        var expectedLines = Stream.of(Stream.of(1, 2, 3, 4, 5).map(BingoNumber::unmarked).toList(),
                                          Stream.of(6, 7, 8, 9, 10).map(BingoNumber::unmarked).toList(),
                                          Stream.of(11, 12, 13, 14, 15).map(BingoNumber::unmarked).toList(),
                                          Stream.of(16, 17, 18, 19, 20).map(BingoNumber::unmarked).toList(),
                                          Stream.of(21, 22, 23, 24, 25).map(BingoNumber::unmarked).toList())
                                  .map(BingoCard.BingoLine::new)
                                  .toList();
        var expectedColumns = Stream.of(Stream.of(1, 6, 11, 16, 21).map(BingoNumber::unmarked).toList(),
                                          Stream.of(2, 7, 12, 17, 22).map(BingoNumber::unmarked).toList(),
                                          Stream.of(3, 8, 13, 18, 23).map(BingoNumber::unmarked).toList(),
                                          Stream.of(4, 9, 14, 19, 24).map(BingoNumber::unmarked).toList(),
                                          Stream.of(5, 10, 15, 20, 25).map(BingoNumber::unmarked).toList())
                                  .map(BingoCard.BingoLine::new)
                                  .toList();

        var expectedSize = expectedLines.size() + expectedColumns.size();
        assertEquals(expectedSize, allLinesAndColumns.size());

        expectedLines.forEach(it -> assertTrue(allLinesAndColumns.contains(it)));
        expectedColumns.forEach(it -> assertTrue(allLinesAndColumns.contains(it)));
    }

    @Test
    void getCardScore() {
        var card = BingoCard.ofLines(BINGO_CARD);

        card.mark(21);
        card.mark(22);
        card.mark(23);
        card.mark(24);
        var finishingNumber = 25;
        card.mark(finishingNumber);
        assertTrue(card.isFinished());

        var cardScore = card.getCardScore();

        var expectedScore = finishingNumber * IntStream.range(1, 21).sum();
        assertEquals(expectedScore, cardScore);
    }

    @Test
    void cardScoreWhileNotFinished() {
        var card = BingoCard.ofLines(BINGO_CARD);
        var expectedUnfinishedScore = -1;
        assertEquals(expectedUnfinishedScore, card.getCardScore());
        card.mark(21);
        card.mark(22);
        card.mark(23);
        card.mark(24);
        assertEquals(expectedUnfinishedScore, card.getCardScore());
        card.mark(25);
        assertNotEquals(expectedUnfinishedScore, card.getCardScore());
    }

}
