package aoc.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ResourceReaderTest {

    @Test
    void readInput() {
        var input = ResourceReader.readInput("1.example");
        assertEquals(10, input.size());
    }

    @Test
    void parseIntegers() {
        var input = ResourceReader.parseIntegers("1.example");
        assertEquals(10, input.size());
    }

}
