package aoc.util;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class UtilTest {

    @Test
    void toSlidingWindowSimple() {
        var in = List.of("A", "B", "C", "D", "E", "F");
        var slidingWindows = Util.toSlidingWindow(in, 1);
        assertEquals(in.size(), slidingWindows.size());
        var reduced = slidingWindows.stream()
                              .flatMap(List::stream)
                              .collect(Collectors.toList());
        assertEquals(in, reduced);
    }

    @Test
    void toSlidingWindow_windowTooBig() {
        var in = List.of("A", "B", "C", "D", "E", "F");
        var slidingWindows = Util.toSlidingWindow(in, 15);
        assertEquals(0, slidingWindows.size());
    }

    @Test
    void toSlidingWindow_windowReasonable() {
        var in = List.of("A", "B", "C", "D", "E", "F");
        var slidingWindows = Util.toSlidingWindow(in, 3);
        assertEquals(4, slidingWindows.size());
        assertEquals(List.of("A", "B", "C"), slidingWindows.get(0));
        assertEquals(List.of("D", "E", "F"), slidingWindows.get(3));
    }

    @Test
    void counting() {
        var in = List.of("A", "B", "A", "B", "C", "A");
        var expectedCounts = Map.of("A", 3L, "B", 2L, "C", 1L);
        var actualCounts = Util.countOccurrences(in);
        assertEquals(expectedCounts, actualCounts);
    }

    @Test
    void mostOccurring() {
        var in = List.of("A", "B", "A", "B", "C", "A");
        var expectedMostOccuring = "A";
        var actualMostOccurring = Util.mostOccurring(in);
        assertEquals(expectedMostOccuring, actualMostOccurring);
    }

    @Test
    void mostOccurringMulti() {
        var in = List.of("A", "B", "A", "B", "C", "D");
        var expectedMostOccuring = Set.of("A", "B");
        var actualMostOccurring = Util.mostOccurringMulti(in);
        assertEquals(expectedMostOccuring, actualMostOccurring);
    }

}
