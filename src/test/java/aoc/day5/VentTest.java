package aoc.day5;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class VentTest {

    @Test
    void isVertical() {
        var p1 = new Point(0, 0);
        var p2 = new Point(0, 5);
        var p3 = new Point(5, 0);
        
        var verticalVent = new Vent(p1, p2);
        var horizontalVent = new Vent(p1, p3);
        assertTrue(verticalVent.isVertical());
        assertFalse(horizontalVent.isVertical());
    }

    @Test
    void isHorizontal() {
        var p1 = new Point(0, 0);
        var p2 = new Point(0, 5);
        var p3 = new Point(5, 0);

        var verticalVent = new Vent(p1, p2);
        var horizontalVent = new Vent(p1, p3);
        assertTrue(horizontalVent.isHorizontal());
        assertFalse(verticalVent.isHorizontal());
    }

    @Test
    void isHorizontalOrVertical() {
        var p1 = new Point(0, 0);
        var p2 = new Point(0, 5);
        var p3 = new Point(5, 0);
        var p4 = new Point(5, 5);
        var verticalVent = new Vent(p1, p2);
        var horizontalVent = new Vent(p1, p3);
        var diagonalVent = new Vent(p1, p4);

        assertTrue(verticalVent.isHorizontalOrVertical());
        assertTrue(horizontalVent.isHorizontalOrVertical());
        assertFalse(diagonalVent.isHorizontalOrVertical());
    }

    @Test
    void toPointsVertical() {
        var p1 = new Point(0, 0);
        var p2 = new Point(0, 5);

        var verticalVent = new Vent(p1, p2);
        var points = verticalVent.toPoints();
        var expectedPoints = Set.of(
                new Point(0, 0),
                new Point(0, 1),
                new Point(0, 2),
                new Point(0, 3),
                new Point(0, 4),
                new Point(0, 5)
        );

        assertEquals(expectedPoints, points);
    }

    @Test
    void toPointsHorizontal() {
        var p1 = new Point(0, 0);
        var p2 = new Point(5, 0);

        var horizontalVent = new Vent(p1, p2);
        var points = horizontalVent.toPoints();
        var expectedPoints = Set.of(
                new Point(0, 0),
                new Point(1, 0),
                new Point(2, 0),
                new Point(3, 0),
                new Point(4, 0),
                new Point(5, 0)
        );

        assertEquals(expectedPoints, points);
    }

    @Test
    void toPointsDiagonal() {

        var p1 = new Point(0, 0);
        var p2 = new Point(5, 5);

        var diagonalVent = new Vent(p1, p2);
        var points = diagonalVent.toPoints();
        var expectedPoints = Set.of(
                new Point(0, 0),
                new Point(1, 1),
                new Point(2, 2),
                new Point(3, 3),
                new Point(4, 4),
                new Point(5, 5)
        );

        assertEquals(expectedPoints, points);
    }

    @Test
    void of() {
        var vent = Vent.of("1,2 -> 3,4");
        assertEquals(1, vent.start().x());
        assertEquals(2, vent.start().y());
        assertEquals(3, vent.end().x());
        assertEquals(4, vent.end().y());
    }

    @Test
    void ofThrows() {
        assertThrows(UnsupportedOperationException.class, () -> Vent.of(""));
        assertThrows(UnsupportedOperationException.class, () -> Vent.of("1,2"));
        assertThrows(UnsupportedOperationException.class, () -> Vent.of("1,2 -> 3,4 -> 5,6"));
    }

}
