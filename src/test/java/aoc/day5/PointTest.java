package aoc.day5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {

    @Test
    void of() {
        var point = Point.of("1,3");
        assertEquals(1, point.x());
        assertEquals(3, point.y());
    }

    @Test
    void ofThrows() {
        assertThrows(UnsupportedOperationException.class, () -> Point.of(""));
        assertThrows(UnsupportedOperationException.class, () -> Point.of("1"));
        assertThrows(UnsupportedOperationException.class, () -> Point.of("1,3,4"));
        assertThrows(NumberFormatException.class, () -> Point.of("a,1"));
        assertThrows(NumberFormatException.class, () -> Point.of("1,a"));
    }

}
