package aoc.day5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VentMapTest {

    private VentMap ventMap;

    @BeforeEach
    void init() {
        ventMap = new VentMap();
    }

    @Test
    void countOverlappingHorizontalVerticalOnExample() {
        ventMap.readInput("5.example");
        var overlappingPointCount = ventMap.countOverlappingHorizontalVerticalVentPoints();
        assertEquals(5, overlappingPointCount);
    }

    @Test
    void countOverlappingHorizontalVerticalOnInput() {
        ventMap.readInput("5.input");
        var overlappingPointCount = ventMap.countOverlappingHorizontalVerticalVentPoints();
        assertEquals(7269, overlappingPointCount);
    }

    @Test
    void countOverlappingOnExample() {
        ventMap.readInput("5.example");
        var overlappingPointCount = ventMap.countOverlappingVentPoints();
        assertEquals(12, overlappingPointCount);
    }

    @Test
    void countOverlappingOnInput() {
        ventMap.readInput("5.input");
        var overlappingPointCount = ventMap.countOverlappingVentPoints();
        assertEquals(7269, overlappingPointCount);
    }


}
